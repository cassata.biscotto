/* Copyright ® 2008-2009 Fulvio Satta
 *
 * If you want contact me, send an email to Yota_VGA@users.sf.net
 *
 * This file is part of Biscotto
 *
 * Biscotto is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Biscotto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef IMAGE_H
#define IMAGE_H
#include <memory> //For auto_ptr
#include <boost/smart_ptr.hpp> //For scoped_array
#include <string>
#include <ImfOutputFile.h>

/* Read and write in an image */
//TODO: Image reading
class Image
{
    public:
        /* A pixel */
        struct RGBA
        {
            float r, g, b, a;

            inline RGBA() : r(0), g(0), b(0), a(0) {}

            inline RGBA(float red, float green, float blue, float alpha) :
                r(red), g(green), b(blue), a(alpha) {}

            inline RGBA &operator+=(const RGBA &ob)
            {
                r += ob.r;
                g += ob.g;
                b += ob.b;
                a += ob.a;

                return *this;
            }

            inline RGBA operator/(double ob) const
            {
                RGBA rgba(r, g, b, a);

                rgba.r /= ob;
                rgba.g /= ob;
                rgba.b /= ob;
                rgba.a /= ob;

                return rgba;
            }
        };

    protected:
        /* Pointers used in the class. Automatic remotion */
        boost::scoped_array<RGBA> data;
        std::auto_ptr<Imf::OutputFile> file;

        /* Size of the image */
        unsigned int m_w, m_h;

    public:
        /* Actions for the class (only image creation for now) */
        enum Action {Create};

        /* Accept file name, the size of the image and the action */
        Image(const std::string &filename, unsigned int w, unsigned int h,
                enum Action action);

        /* Write the next n lines, from top to bottom */
        void writeLines(unsigned int n);

        /* Like writeLines(1) */
        void writeLine();

        /* Get the pointer of a pixel column.
         * It is possible access to the RGBA structure with image[x][y] */
        inline RGBA *operator[](unsigned int x)
        {
            return &data[x * m_h];
        }

        /* Like the previous, but for consts */
        inline const RGBA *operator[](unsigned int x) const
        {
            return &data[x * m_h];
        }

        /* Sizes of the image */
        inline unsigned int w() const
        {
            return m_w;
        }

        inline unsigned int h() const
        {
            return m_h;
        }
};

#endif
