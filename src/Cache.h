//vim: expandtab:shiftwidth=4:fileencoding=utf-8 :

/* Copyright ® 2008-2009 Fulvio Satta

   If you want contact me, send an email to Yota_VGA@users.sf.net

   This file is part of Biscotto

   Biscotto is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Biscotto is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef CACHE_H
#define CACHE_H

#include <set>
#include <map>
#include <boost/pool/pool_alloc.hpp>
#include <boost/thread.hpp>

class CacheObject;

/* Cache system
 *
 * The cache system work correctly only if the ids of the protected objects
 * are all in the unsigned long int intervall size and if the ++ overflow of
 * the unsigned long int give 0.
 * This is sure in all the pratical cases.
 */
class Cache
{
    friend class CacheObject;

    protected:
        /* Functor for the monitor of the cache */
        class Monitor
        {
            public:
                Monitor() {}

            void operator()();
        };

        /* Internal rappresentation of the cache */
        static Cache m_cache;

        /* Syncronization variables */
        mutable boost::mutex mutex;
        boost::condition_variable command;
        boost::condition_variable_any waitdelfirst;

        /* State of the cache */
        unsigned long int position;
        unsigned long int size;
        unsigned long int first;
        bool delfirst;

        /* Maximum size of the cache, not strictly followed */
        unsigned long int max;

        /* Functor for the map order */
        struct circular_order
        {
            inline bool operator()(unsigned long int a, unsigned long int b)
            {
                if (a < cache().position and b >= cache().position)
                    return true;

                return a < b;
            }
        };

        /* Order of the objects */
        std::map<unsigned long int, CacheObject *,
                 circular_order,
                 boost::pool_allocator<
                     std::pair<unsigned long int, CacheObject *> > >
            order;

        /* Object collection */
        std::set<CacheObject *, std::less<CacheObject *>,
                 boost::pool_allocator<CacheObject *> >
            objects;

        /* Internal costructor, is a singleton */
        Cache();

        /* Add an object in the cache */
        void add(CacheObject &object);

        /* Delete an object in the cache */
        void del(CacheObject &object);

        /* Resize an object */
        void resize(long int delta);

        /* Call this if an object is used */
        void use(CacheObject &object);

    public:
        /* Return the cache (singleton) */
        static inline Cache &cache()
        {
            return m_cache;
        }

        /* Set the cache limit (not strictly) */
        void setLimit(unsigned long int imit);

        /* Get the cache limit (not strictly) */
        unsigned long int limit() const;

        CacheObject *readPointer(CacheObject **ptr) const;
};

#include "CacheObject.h"

#endif
