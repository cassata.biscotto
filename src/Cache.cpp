//vim: expandtab:shiftwidth=4:fileencoding=utf-8 :

/* Copyright ® 2009 Fulvio Satta

   If you want contact me, send an email to Yota_VGA@users.sf.net

   This file is part of Biscotto

   Biscotto is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Biscotto is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "Cache.h"
#include <boost/lambda/lambda.hpp>

Cache Cache::m_cache;

void Cache::Monitor::operator()()
{
    FAST_LOCK_O(cache().mutex);

    for (;;)
    {
        /* Iterate if the cache must delete some element */
        for (typeof(cache().order.begin()) i = cache().order.begin();
             i != cache().order.end() and (cache().size >= cache().max or
                                           cache().delfirst);
             i++)
        {
            /* Delete some object */
            CacheObject *object = i->second;
            if (not object->destroy())
                continue;

            delete object;

            cache().delfirst = false;
        }

        /* Rescan the first index */
        cache().first = cache().order.begin()->first;

        /* Notify that the first is deleted */
        guard.unlock();
        cache().waitdelfirst.notify_all();

        /* Wait for the next cache optimization */
        guard.lock();
        cache().command.wait(guard);
    }
}

Cache::Cache() : position(0), first(0), delfirst(false), max(1024 * 1024 * 10)
{
    /* Launch the thread monitor */
    boost::thread(Monitor());
}

void Cache::add(CacheObject &object)
{
    FAST_LOCK_O(mutex);

    /* Wait if the first must be deleted */
    waitdelfirst.wait(guard, boost::lambda::var(delfirst));

    /* Add the new object */
    object.setPosition(position);
    order[position++] = &object;
    objects.insert(&object);

    /* Update the size */
    size += object.size();

    /* If the first must be deleted */
    if (position == first)
        delfirst = true;

    /* If the cache should be optimized */
    if (size >= max or delfirst)
    {
        guard.unlock();
        command.notify_one();
    }
}

void Cache::del(CacheObject &object)
{
    size -= object.size();
    order.erase(object.position());
    objects.erase(&object);
}

void Cache::resize(long int delta)
{
    FAST_LOCK_O(mutex);

    size += delta;

    /* If the size should be optimized */
    if (size >= max)
    {
        guard.unlock();
        command.notify_one();
    }
}

void Cache::use(CacheObject &object)
{
    FAST_LOCK_O(mutex);

    /* Wait if the first must be deleted */
    waitdelfirst.wait(guard, boost::lambda::var(delfirst));

    order.erase(object.position());
    object.setPosition(position++);
    order[position] = &object;
}

void Cache::setLimit(unsigned long int limit)
{
    FAST_LOCK_O(mutex);

    max = limit;
}

unsigned long int Cache::limit() const
{
    FAST_LOCK_O(mutex);

    return max;
}

CacheObject *Cache::readPointer(CacheObject **ptr) const
{
    FAST_LOCK_O(mutex);

    (*ptr)->incprotect();

    return *ptr;
}
