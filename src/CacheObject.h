//vim: expandtab:shiftwidth=4:fileencoding=utf-8 :

/* Copyright ® 2008-2009 Fulvio Satta

   If you want contact me, send an email to Yota_VGA@users.sf.net

   This file is part of Biscotto

   Biscotto is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Biscotto is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#ifndef CACHEOBJECT
#define CACHEOBJECT

#include <boost/thread.hpp>
#include "Cache.h"

/* Sincronization helper */
#define FAST_LOCK_N(o, n) boost::unique_lock<typeof(o)> n(o)
#define FAST_LOCK_O(o)    FAST_LOCK_N(o, guard)
#define FAST_LOCK         FAST_LOCK_O(*this)

#define READ_LOCK_N(o, n) boost::shared_lock<typeof(o)> n(o)
#define READ_LOCK_O(o)    READ_LOCK_N(o, guard)
#define READ_LOCK         READ_LOCK_N(*this)

/* Base class for the cachable objects */
class CacheObject
{
    friend class Cache;

    protected:
        /* Position in cache order */
        unsigned long int m_position;

        /* Internal size */
        unsigned long int m_size;

        /* If the object is protected */
        unsigned long int protect;

        /* For sincronizzation */
        mutable boost::shared_mutex mutex;

        /* Get the position in cache order */
        inline unsigned int position() const
        {
            FAST_LOCK;

            return m_position;
        }

        /* Set the position in cache order */
        inline void setPosition(unsigned int pos)
        {
            FAST_LOCK;

            m_position = pos;
        }

        /* Set the new object size */
        void resize(unsigned long int size)
        {
            FAST_LOCK;

            Cache::cache().resize(size - m_size);
            m_size = size;
        }

        /* Called when the object should be destroyed in the cache
         * Return true if the object will be destroyed, false in the other
         * cases
         */
        virtual bool destroy()
        {
            return not protect;
        }

        /* Constructor that require the initial object size */
        inline CacheObject(unsigned long int size) : m_position(),
                                                     m_size(size),
                                                     protect(0)
        {
            Cache::cache().add(*this);
        }

        void incprotect()
        {
            FAST_LOCK;

            protect++;
        }

    public:
        /* Return the actual size of the object */
        unsigned long int size() const
        {
            return m_size;
        }

        /* Sign an object like used, for the cache */
        void use()
        {
            Cache::cache().use(*this);
        }

        /* Protect or unprotect an object */
        inline void unprotect()
        {
            FAST_LOCK;

            protect--;
        }

        /* Functions for the lockable boost concept */
        inline void lock() const
        {
            mutex.lock();
        }

        inline bool try_lock() const
        {
            return mutex.try_lock();
        }

        inline void unlock() const
        {
            mutex.unlock();
        }

        /* Functions for the shared lockable boost concept */
        inline void lock_shared()
        {
            mutex.lock_shared();
        }

        inline bool try_lock_shared()
        {
            return mutex.try_lock_shared();
        }

        inline void unlock_shared()
        {
            return mutex.unlock_shared();
        }

        /* Delete the object */
        ~CacheObject()
        {
            Cache::cache().del(*this);
        }
};

#endif
